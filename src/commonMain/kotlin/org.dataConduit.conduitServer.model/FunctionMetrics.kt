package org.dataConduit.conduitServer.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
/** Represents metrics for a Serverless Function. */
internal data class FunctionMetrics(
    @SerialName("func-name")
    /** Name of the Serverless Function. */
    val funcName: String,
    /** Process state as text. */
    val state: String,
    /** Process identifier. */
    val pid: UInt,
    @SerialName("file-mem")
    /** Amount of memory used with files and devices in KB. */
    val fileMem: ULong,
    @SerialName("reserved-mem")
    /** Amount of reserved memory in KB. */
    val reservedMem: ULong,
    @SerialName("total-threads")
    /** Number of threads used by the process. */
    val totalThreads: UInt,
    @SerialName("core-dump-enabled")
    /** If enabled then a core dump occurs when the process crashes. */
    val coreDumpEnabled: Boolean
)
