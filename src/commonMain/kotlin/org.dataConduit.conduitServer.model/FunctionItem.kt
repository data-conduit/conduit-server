package org.dataConduit.conduitServer.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class FunctionItem(val pid: UInt, @SerialName("func-name") val funcName: String)
