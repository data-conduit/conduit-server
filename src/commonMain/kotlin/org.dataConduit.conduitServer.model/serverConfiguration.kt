package org.dataConduit.conduitServer.model

import org.dataConduit.conduitServer.extractMaxFunctionDuration
import org.dataConduit.conduitServer.extractPort
import kotlin.time.Duration

internal data class ServerConfiguration(val port: UInt, val maxFuncDuration: Duration) {
    companion object {
        fun fromArguments(args: Array<String>): ServerConfiguration {
            val port = extractPort(args)
            val maxFuncDuration = extractMaxFunctionDuration(args)
            return ServerConfiguration(port = port, maxFuncDuration = maxFuncDuration)
        }
    }
}

internal fun ServerConfiguration.printConfiguration() {
    println(
        """
        -- Server Configuration --
        * Port: $port
        * Maximum Serverless Function Duration: ${maxFuncDuration.inWholeSeconds} sec
        """.trimIndent()
    )
}
