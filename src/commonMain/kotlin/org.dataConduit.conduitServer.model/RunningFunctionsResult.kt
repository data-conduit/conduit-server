package org.dataConduit.conduitServer.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class RunningFunctionsResult(
    @SerialName("running-functions")
    val runningFunctions: List<FunctionItem>
)
