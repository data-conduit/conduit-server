package org.dataConduit.conduitServer.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal sealed interface ResponseError {
    @SerialName("error-msg") val errorMsg: String
}

@Serializable
internal data class InvalidFunctionName(@SerialName("error-msg") override val errorMsg: String) : ResponseError

@Serializable
internal data class FunctionNotFound(
    @SerialName("func-name")
    val funcName: String,
    @SerialName("error-msg")
    override val errorMsg: String
) : ResponseError

@Serializable
internal data class FunctionAlreadyInstalled(
    @SerialName("func-name")
    val funcName: String,
    @SerialName("error-msg")
    override val errorMsg: String
) : ResponseError

@Serializable
internal data class EmptyData(@SerialName("error-msg") override val errorMsg: String) : ResponseError

@Serializable
internal data class FunctionNotRunning(
    @SerialName("func-name") val
    funcName: String,
    @SerialName("error-msg")
    override val errorMsg: String
) : ResponseError

@Serializable
internal data class GeneralFunctionError(
    @SerialName("func-name")
    val funcName: String,
    @SerialName("exit-code")
    val exitCode: Int,
    @SerialName("error-msg")
    override val errorMsg: String
) : ResponseError

@Serializable
internal data class FunctionTimeout(
    @SerialName("func-name")
    val funcName: String,
    @SerialName("max-func-duration")
    /** Maximum function duration in seconds. */
    val maxFuncDuration: ULong,
    @SerialName("error-msg")
    override val errorMsg: String
) : ResponseError
