package org.dataConduit.conduitServer

import org.dataConduit.conduitServer.model.FunctionItem
import kotlin.time.Duration.Companion.seconds

private typealias FuncList = MutableList<FunctionItem>

private val runningFunctions: FuncList = mutableListOf()

internal fun addRunningFunction(pid: UInt, funcName: String) {
    runningFunctions += FunctionItem(pid, funcName)
}

internal fun removeRunningFunction(pid: UInt) {
    val list = runningFunctions.filter { it.pid == pid }
    if (list.size == 1) runningFunctions -= list.first()
}

internal fun listRunningFunctions() = runningFunctions.toList()

internal fun extractMaxFunctionDuration(args: Array<String>) = try {
    val tmp = args.singleOrNull { it.startsWith("--maxFuncDuration=") }
        ?.replace("--maxFuncDuration=", "")
        ?.toUInt() ?: 30u
    tmp.toInt().seconds

} catch (ex: NumberFormatException) {
    30.seconds
}

internal fun extractPort(args: Array<String>) = try {
    args.singleOrNull { it.startsWith("--port=") }
        ?.replace("--port=", "")
        ?.toUInt()
        ?: 8080u
} catch (ex: NumberFormatException) {
    8080u
}

private fun String.hasInvalidCharacters() = !toCharArray().any { it.isLetter() || it.isDigit() || it == '_' }

@Suppress("ReplaceRangeToWithUntil")
internal fun validFunctionName(name: String) = when {
    name.isEmpty() -> false
    name.isBlank() -> false
    name.length < 3 -> false
    !name.first().isLetter() -> false
    name.slice(1..(name.length - 1)).hasInvalidCharacters() -> false
    else -> true
}
