package org.dataConduit.conduitServer

import kotlinx.cinterop.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.dataConduit.conduitServer.io.Path
import org.dataConduit.conduitServer.io.listDirectories
import org.dataConduit.conduitServer.io.removeFile
import org.dataConduit.conduitServer.io.toPath
import platform.posix.*
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

internal fun errorAsString() = strerror(errno)?.toKString() ?: ""

internal fun runProgram(programPath: Path, args: Array<String>) = memScoped {
    val argvSize = args.size + 2
    val argv = allocArray<CPointerVar<ByteVar>>(argvSize)
    argv[0] = programPath.value.cstr.ptr
    args.forEachIndexed { pos, item -> argv[pos + 1] = item.cstr.ptr }
    argv[argvSize - 1] = null
    execvp(programPath.value, argv)
}

internal suspend fun runServerlessFunction(
    name: String,
    args: Array<String>,
    maxDuration: Duration
): Pair<Int, Array<String>> {
    val outputFile = "/tmp/conduit_server_${name}_output".toPath()
    val namespaceArgs = NamespaceArguments(
        programPath = "$functionsDir/$name/$name".toPath(),
        programArgs = args.toList(),
        outputFilePath = outputFile,
        maxFuncDuration = maxDuration
    )
    val result = runInNamespace(namespaceArgs)
    removeFile(outputFile)
    return result
}

internal fun fetchBinaryPath(pid: UInt): Path = memScoped {
    val buf = allocArray<ByteVar>(255)
    readlink(__len = 255u, __path = "/proc/$pid/exe", __buf = buf)
    buf.toKString().toPath()
}

internal suspend fun runProcessTimer(
    pid: UInt,
    binaryName: String,
    duration: Duration
) = coroutineScope {
    launch(Dispatchers.Default) {
        var counter = 0.seconds
        while (counter < duration) {
            delay(1.seconds)
            counter += 1.seconds
            if (!processRunning(pid, binaryName)) break
            if (counter == duration && pid > 0u) terminateProcess(pid)
        }
    }
}

private fun terminateProcess(pid: UInt) {
    kill(pid.toInt(), SIGKILL)
    removeRunningFunction(pid)
}

private fun processRunning(pid: UInt, binaryName: String): Boolean = when {
    "$pid" !in listDirectories("/proc".toPath()) -> false
    fetchBinaryPath(pid).fileName != binaryName -> false
    else -> true
}
