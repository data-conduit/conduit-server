package org.dataConduit.conduitServer

import org.dataConduit.conduitServer.io.Path
import kotlin.time.Duration

internal data class NamespaceArguments(
    val programPath: Path,
    val programArgs: List<String>,
    val outputFilePath: Path,
    val maxFuncDuration: Duration
)
