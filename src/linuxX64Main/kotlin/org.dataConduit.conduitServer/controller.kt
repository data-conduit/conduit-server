package org.dataConduit.conduitServer

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.utils.io.*
import io.ktor.utils.io.charsets.*
import org.dataConduit.conduitServer.io.*
import platform.posix.S_IRWXG
import platform.posix.S_IRWXU
import platform.posix.getpid

private val parentDir = fetchBinaryPath(getpid().toUInt()).parentDir.toPath()
internal actual val functionsDir = "$parentDir/functions".toPath()

internal actual suspend fun installFunction(name: String, channel: ByteReadChannel, size: UInt) {
    createFunctionDirectory(name)
    val data = channel.readBinary(size)
    createBinaryFile(functionsDir.appendItems(name, name), data)
}

internal actual fun uninstallFunction(name: String) {
    deleteDirectory(functionsDir.appendItems(name))
}

internal actual fun functionExists(name: String): Boolean = name in listDirectories(functionsDir)

internal actual fun listInstalledFunctions(): Array<String> = listDirectories(functionsDir)

internal actual fun createFunctionDirectory(name: String) {
    val userMode = S_IRWXU.toUInt()
    val groupMode = S_IRWXG.toUInt()
    createDirectory("$functionsDir/$name".toPath(), userMode or groupMode)
}

internal actual suspend fun ByteReadChannel.readBinary(size: UInt): ByteArray {
    val result = ByteArray(size.toInt())
    var offset = 0
    var currentRead: Int
    do {
        currentRead = readAvailable(dst = result, offset = offset, length = result.size)
        offset += currentRead
    } while (currentRead > 0)
    return result
}

internal suspend inline fun ApplicationCall.respondStatic(path: Path) {
    if (path.fileExists()) {
        respond(LocalFileContent(path, ContentType.defaultForFile(path)))
    }
}

internal fun ContentType.Companion.defaultForFile(path: Path): ContentType =
    ContentType.fromFileExtension(path.fileExt).selectDefault()

internal fun List<ContentType>.selectDefault(): ContentType {
    val textType = "text"
    val jsType = "javascript"
    val contentType = firstOrNull() ?: ContentType.Application.OctetStream
    return when {
        contentType.contentType == textType && contentType.charset() == null -> contentType.withCharset(Charsets.UTF_8)
        contentType.contentSubtype == jsType && contentType.charset() == null -> contentType.withCharset(Charsets.UTF_8)
        else -> contentType
    }
}
