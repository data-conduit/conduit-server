package org.dataConduit.conduitServer

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.util.*
import io.ktor.utils.io.errors.*
import org.dataConduit.conduitServer.io.*
import org.dataConduit.conduitServer.io.directoryExists
import org.dataConduit.conduitServer.io.listDirectories
import org.dataConduit.conduitServer.model.*
import kotlin.time.Duration

private val staticRootFolderKey = AttributeKey<Path>("BaseFolder")

internal fun Application.setupRouting(conf: ServerConfiguration) = routing {
    setupHomeRoute()
    setupRunFunctionRoute(conf.maxFuncDuration)
    setupListRunningFunctionsRoute()
    setupListInstalledFunctionsRoute()
    setupFunctionMetricsRoute()
    setupInstallFunctionRoute()
    setupUninstallFunctionRoute()
}

private fun Route.setupUninstallFunctionRoute() = post("/function/uninstall/{name}") {
    val funcName = call.parameters["name"] ?: ""
    if (!validFunctionName(funcName)) {
        call.respond(HttpStatusCode.BadRequest, InvalidFunctionName("Invalid function name"))
    } else if (!functionExists(funcName)) {
        val msg = FunctionNotFound(funcName, "Serverless Function doesn't exist")
        call.respond(HttpStatusCode.Conflict, msg)
    } else {
        uninstallFunction(funcName)
        call.respondText("Serverless Function $funcName uninstalled successfully")
    }
}

private fun Route.setupListInstalledFunctionsRoute() = get("/function/all/list-installed") {
    val output = try {
        listInstalledFunctions()
    } catch (ioEx: IOException) {
        emptyArray()
    }
    call.respond(output)
}

private fun Route.setupInstallFunctionRoute() = post("/function/install/{name}") {
    val funcName = call.parameters["name"] ?: ""
    val contentLength = (call.request.header("Content-Length")?.toIntOrNull()) ?: 0
    if (!validFunctionName(funcName)) {
        call.respond(HttpStatusCode.BadRequest, InvalidFunctionName("Invalid function name"))
    } else if (functionExists(funcName)) {
        val msg = FunctionAlreadyInstalled(funcName, "Serverless Function is already installed")
        call.respond(HttpStatusCode.Conflict, msg)
    } else if (contentLength == 0) {
        call.respond(HttpStatusCode.BadRequest, EmptyData("Binary data is empty"))
    } else {
        installFunction(name = funcName, channel = call.receiveChannel(), size = contentLength.toUInt())
        call.respondText("Serverless Function $funcName installed successfully")
    }
}

private fun Route.setupListRunningFunctionsRoute() = get("/function/all/list-running") {
    call.respond(RunningFunctionsResult(listRunningFunctions()))
}

private fun Route.setupFunctionMetricsRoute() = get("/function/metrics/{name}") {
    val funcName = call.parameters["name"] ?: ""
    if (!functionsDir.directoryExists() || funcName !in listDirectories(functionsDir)) {
        val msg = FunctionNotFound(funcName, "Serverless Function not found")
        call.respond(HttpStatusCode.Conflict, msg)
    } else {
        try {
            val metrics = fetchFunctionMetrics(funcName)
            call.respond(metrics)
        } catch (ex: Exception) {
            val msg = FunctionNotRunning(funcName, "Serverless Function isn't running")
            call.respond(HttpStatusCode.Conflict, msg)
        }
    }
}

private fun Route.setupRunFunctionRoute(maxDuration: Duration) = post("/function/run/{name}") {
    val contentLength = (call.request.header("Content-Length")?.toIntOrNull()) ?: 0
    val requestBody = if (contentLength > 0) call.receiveText() else ""
    val funcArgs =
        if (requestBody.isEmpty()) emptyArray() else requestBody.split("\n").toTypedArray()
    val funcName = call.parameters["name"] ?: ""
    if (!functionsDir.directoryExists() || funcName !in listDirectories(functionsDir)) {
        val msg = FunctionNotFound(funcName, "Serverless Function not found")
        call.respond(HttpStatusCode.Conflict, msg)
    } else {
        val (exitCode, output) = runServerlessFunction(name = funcName, args = funcArgs, maxDuration = maxDuration)
        val responseTxt = buildString {
            output.forEach { append("$it\n") }
        }
        sendRunFunctionResponse(funcName = funcName, appCall = call, exitCode = exitCode, responseTxt = responseTxt,
            maxFuncDuration = maxDuration)
    }
}

private suspend fun sendRunFunctionResponse(
    funcName: String,
    appCall: ApplicationCall,
    exitCode: Int,
    responseTxt: String,
    maxFuncDuration: Duration
) {
    when (exitCode) {
        0 -> {
            appCall.respondText(text = responseTxt, status = HttpStatusCode.OK)
        }
        9 -> {
            val msg = FunctionTimeout(
                funcName = funcName,
                errorMsg = "Serverless Function terminated (took too long to run)",
                maxFuncDuration = maxFuncDuration.inWholeSeconds.toULong()
            )
            appCall.respond(HttpStatusCode.Conflict, msg)
        }
        else -> {
            val msg = GeneralFunctionError(
                funcName = funcName,
                errorMsg = "Unknown error with Serverless Function",
                exitCode = exitCode
            )
            appCall.respond(HttpStatusCode.Conflict, msg)
        }
    }
}

private fun Route.setupHomeRoute() = route("$DIR_SEPARATOR") {
    this@setupHomeRoute.staticRootFolder = "static".toPath()
    this@setupHomeRoute.singleFile("index.html", "index.html")
    this@setupHomeRoute.default("index.html")

    this@setupHomeRoute.route("assets") {
        files("css")
        files("js")
    }
}

private fun Route.files(folder: String) {
    val dir = staticRootFolder?.appendItems(folder) ?: return
    val pathParameter = "static-content-path-parameter"
    get("{$pathParameter...}") {
        val relativePath = call.parameters.getAll(pathParameter)?.joinToString("$DIR_SEPARATOR") ?: return@get
        val file = dir.appendItems(relativePath)
        call.respondStatic(file)
    }
}

private fun Route.default(localPath: String) {
    val path = staticRootFolder?.appendItems(localPath) ?: return
    get { call.respondStatic(path) }
}

private var Route.staticRootFolder: Path?
    get() = attributes.getOrNull(staticRootFolderKey) ?: parent?.staticRootFolder
    set(value) {
        if (value != null) attributes.put(staticRootFolderKey, value)
        else attributes.remove(staticRootFolderKey)
    }

private fun Route.singleFile(remotePath: String, localPath: String) {
    val path = staticRootFolder?.appendItems(localPath) ?: return
    get(remotePath) { call.respondStatic(path) }
}
