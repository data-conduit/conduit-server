package org.dataConduit.conduitServer

import io.ktor.utils.io.errors.*
import org.dataConduit.conduitServer.io.close
import org.dataConduit.conduitServer.io.readAllByLines
import org.dataConduit.conduitServer.model.FunctionMetrics
import platform.posix.fopen

internal fun fetchFunctionMetrics(funcName: String): FunctionMetrics {
    val tmp = listRunningFunctions().filter { it.funcName == funcName }
    if (tmp.isEmpty()) throw IllegalStateException("An instance of the Serverless Function must be running")
    val pid = tmp.first().pid
    val procInfo = readProcInfo(pid)
    return createFunctionMetrics(funcName = funcName, pid = pid, procInfo = procInfo)
}

private fun createFunctionMetrics(funcName: String, pid: UInt, procInfo: Array<String>): FunctionMetrics {
    var state = ""
    var totalThreads = 0u
    var coreDumpEnabled = false
    var reservedMem = 0uL
    var fileMem = 0uL

    procInfo.forEach { line ->
        if (line.startsWith("State:")) state = extractState(line)
        if (line.startsWith("Threads:")) totalThreads = extractThreads(line)
        if (line.startsWith("RssAnon:")) reservedMem = extractReservedMemory(line)
        if (line.startsWith("RssFile:")) fileMem = extractFileMemory(line)
        if (line.startsWith("CoreDumping:")) coreDumpEnabled = extractCoreDumping(line)
    }

    return FunctionMetrics(
        funcName = funcName,
        pid = pid,
        state = state,
        totalThreads = totalThreads,
        coreDumpEnabled = coreDumpEnabled,
        reservedMem = reservedMem,
        fileMem = fileMem
    )
}

private fun readProcInfo(pid: UInt): Array<String> {
    val file = fopen("/proc/$pid/status", "r")
        ?: throw IOException("Cannot open file: ${errorAsString()}")
    val result = file.readAllByLines()
    file.close()
    return result
}

private fun extractState(line: String) = line.replace("State:", "").trim()

private fun extractThreads(line: String) = line.replace("Threads:", "").trim().toUInt()

private fun extractCoreDumping(line: String) = line
    .replace("CoreDumping:", "")
    .trim()
    .toUInt()
    .booleanValue

private fun extractReservedMemory(line: String) = line
    .replace("RssAnon:", "")
    .replace("kB", "")
    .trim()
    .toULong()

private fun extractFileMemory(line: String) = line
    .replace("RssFile:", "")
    .replace("kB", "")
    .trim()
    .toULong()

private val UInt.booleanValue: Boolean
    get() = this != 0u
