package org.dataConduit.conduitServer.io

import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.util.date.*
import io.ktor.utils.io.*
import org.dataConduit.conduitServer.defaultForFile

internal class LocalFileContent(
    private val path: Path,
    override val contentType: ContentType = ContentType.defaultForFile(path)
) : OutgoingContent.WriteChannelContent() {
    private val metadata = path.metadata
    override val contentLength: Long get() = metadata.totalSize

    override suspend fun writeTo(channel: ByteWriteChannel) {
        val textContentType = "text"
        val jsContentType = "javascript"
        val file = if (contentType.contentType == textContentType || contentType.contentSubtype == jsContentType) {
            openFile(path, "r")
        } else {
            openFile(path, "rb")
        }

        val bufferSize = 4uL * 1024uL
        while (true) {
            val buffer = file.readBytes(bufferSize)
            if (buffer.isEmpty()) break
            channel.writeFully(buffer)
        }
        file.close()
    }

    init {
        if (!path.fileExists()) throw IllegalStateException("No such file $path")
        val modificationTime = metadata.lastModificationTime
        @Suppress("SuspiciousCollectionReassignment")
        if (modificationTime > 0) versions += LastModifiedVersion(GMTDate(modificationTime))
    }
}
