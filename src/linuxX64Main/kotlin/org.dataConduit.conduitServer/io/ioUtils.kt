package org.dataConduit.conduitServer.io

import io.ktor.utils.io.errors.*
import kotlinx.cinterop.*
import org.dataConduit.conduitServer.errorAsString
import platform.posix.*

private val dirFileType = DT_DIR.toUByte()

internal fun openFile(filePath: Path, modes: String) = fopen(filePath.value, modes)

internal actual fun createDirectory(dirPath: Path, mode: UInt) {
    mkdir(dirPath.value, mode)
}

internal actual fun removeFile(filePath: Path) {
    remove(filePath.value)
}

internal actual fun deleteDirectory(dirPath: Path) {
    val excluded = setOf(".", "..")
    val dir = opendir(dirPath.value) ?: throw IOException("Cannot open directory: ${errorAsString()}")
    var entry: CPointer<dirent>?
    do {
        entry = readdir(dir)
        val name = entry?.pointed?.d_name?.toKString() ?: ""
        val fileType = entry?.pointed?.d_type
        if (name !in excluded && fileType == dirFileType) deleteDirectory(dirPath.appendItems(name))
        else if (name !in excluded) remove("$dirPath/$name")
    } while (entry != null)
    remove(dirPath.value)
    dir.close()
}

internal actual fun listDirectories(dirPath: Path): Array<String> {
    val excluded = setOf(".", "..")
    val tmp = mutableListOf<String>()
    val dir = opendir(dirPath.value) ?: throw IOException("Cannot open directory: ${errorAsString()}")
    var entry: CPointer<dirent>?
    do {
        entry = readdir(dir)
        val name = entry?.pointed?.d_name?.toKString() ?: ""
        val fileType = entry?.pointed?.d_type
        if (name !in excluded && fileType == dirFileType) tmp += name
    } while (entry != null)
    dir.close()
    return tmp.toTypedArray()
}

internal actual fun createBinaryFile(filePath: Path, data: ByteArray) {
    val file = fopen(filePath.value, "wb")
    file.writeBinary(data)
    file.close()
    val userMode = S_IRUSR.toUInt() or S_IXUSR.toUInt()
    val groupMode = S_IRGRP.toUInt() or S_IXGRP.toUInt()
    chmod(filePath.value, userMode or groupMode)
}

internal fun CPointer<FILE>?.writeBinary(data: ByteArray) = memScoped {
    data.usePinned { pin ->
        fwrite(__s = this@writeBinary, __n = 1, __size = data.size.toULong(), __ptr = pin.addressOf(0))
    }
}

internal fun CPointer<FILE>?.writeLine(data: String) = memScoped {
    val fileData = if (data == "\n") data else "$data\n"
    fwrite(__s = this@writeLine, __n = 1, __size = fileData.length.toULong(), __ptr = fileData.cstr)
}

private fun CArrayPointer<ByteVar>.toByteArray(size: Int): ByteArray {
    val result = ByteArray(size)
    @Suppress("ReplaceRangeToWithUntil")
    (0..(size - 1)).forEach { result[it] = this[it] }
    return result
}

internal fun CPointer<FILE>?.readBytes(bufferSize: ULong) = memScoped {
    require(this@readBytes != null) { "The receiver cannot be null" }
    val buffer = allocArray<ByteVar>(bufferSize.toInt())
    val bytesRead = fread(__stream = this@readBytes, __n = bufferSize, __ptr = buffer,
        __size = sizeOf<ByteVar>().toULong())
    buffer.toByteArray(bytesRead.toInt())
}

internal fun CPointer<FILE>?.readAllByLines() = memScoped {
    require(this@readAllByLines != null) { "The receiver cannot be null" }
    val tmp = mutableListOf<String>()
    val line = alloc<CPointerVar<ByteVar>>()
    val size = alloc<ULongVar>()
    var bytesRead: Long
    do {
        bytesRead = getline(__stream = this@readAllByLines, __n = size.ptr, __lineptr = line.ptr)
        if (bytesRead > 0L) {
            tmp += (line.value?.toKString() ?: "").replace("\n", "")
        }
    } while (bytesRead > 0L)
    tmp.toTypedArray()
}

internal fun CPointer<FILE>?.readLines(limit: UInt = 1u) = memScoped {
    require(limit > 0u) { "The limit parameter must be greater than 0" }
    require(this@readLines != null) { "The receiver cannot be null" }
    val tmp = mutableListOf<String>()
    val line = alloc<CPointerVar<ByteVar>>()
    val size = alloc<ULongVar>()
    var bytesRead: Long
    var pos = 0u
    do {
        bytesRead = getline(__stream = this@readLines, __n = size.ptr, __lineptr = line.ptr)
        if (bytesRead > 0L) {
            tmp += (line.value?.toKString() ?: "").replace("\n", "")
        }
        pos++
    } while (pos < limit)
    tmp.toTypedArray()
}

internal fun CPointer<FILE>?.close() {
    if (this != null) fclose(this)
}

internal fun CPointer<DIR>?.close() {
    if (this != null) closedir(this)
}

internal fun createPipe(fileDescriptors: CArrayPointer<IntVar>) {
    if (pipe(fileDescriptors) != 0) throw IOException("Cannot create pipe: ${errorAsString()}")
}

internal fun openFileDescriptor(fileDescriptor: Int, modes: String = "r") =
    fdopen(fileDescriptor, modes)
        ?: throw IOException("Cannot open file descriptor: ${errorAsString()}")

internal actual fun duplicateFileDescriptor(oldFd: Int, newFd: Int) {
    if (dup2(oldFd, newFd) == -1) throw IOException("Cannot duplicate file descriptor: ${errorAsString()}")
}
