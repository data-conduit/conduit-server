package org.dataConduit.conduitServer.io

import kotlinx.cinterop.alloc
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.ptr
import platform.posix.opendir
import platform.posix.stat

internal actual fun Path.fileExists(): Boolean = memScoped {
    val stat = alloc<stat>()
    stat(value, stat.ptr) == 0
}

internal actual fun Path.directoryExists(): Boolean {
    var result = false
    val dir = opendir(value)
    if (dir != null) {
        result = true
        dir.close()
    }
    return result
}

internal actual val Path.metadata: FileMetadata
    get() = memScoped {
        val stat = alloc<stat>()
        if (stat(value, stat.ptr) == 0) stat.toFileMetadata()
        else emptyFileMetadata()
    }

private fun stat.toFileMetadata() = FileMetadata(
    userId = st_uid,
    groupId = st_gid,
    devId = st_dev,
    hardLinks = st_nlink,
    mode = st_mode,
    inodeNum = st_ino,
    specialDevId = st_rdev,
    totalSize = st_size,
    blockSize = st_blksize,
    totalBlocks = st_blocks,
    lastAccessTime = st_atim.tv_sec,
    lastModificationTime = st_mtim.tv_sec,
    lastStatusChangeTime = st_ctim.tv_sec
)
