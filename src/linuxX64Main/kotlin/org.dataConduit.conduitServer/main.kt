package org.dataConduit.conduitServer

import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.cio.*
import io.ktor.server.engine.*
import org.dataConduit.conduitServer.model.ServerConfiguration
import org.dataConduit.conduitServer.model.printConfiguration

fun main(args: Array<String>) {
    println("Starting Conduit Server...")
    val conf = ServerConfiguration.fromArguments(args)
    conf.printConfiguration()
    embeddedServer(factory = CIO, port = conf.port.toInt()) {
        install(ContentNegotiation) { json() }
        setupRouting(conf)
    }.start(true)
}
