package org.dataConduit.conduitServer

import kotlinx.cinterop.*
import org.dataConduit.conduitServer.io.*
import org.dataConduit.conduitServer.io.createPipe
import org.dataConduit.conduitServer.io.duplicateFileDescriptor
import org.dataConduit.conduitServer.io.openFileDescriptor
import platform.linux.free
import platform.posix.*
import platform.scheduler.clone_process

// Use 65 KB for the stack size.
private const val STACK_SIZE = 65536uL
// New pid namespace.
private const val CLONE_NEWPID = 0x20000000
// New utsname group.
private const val CLONE_NEWUTS = 0x04000000
// New user namespace.
private const val CLONE_NEWUSER = 0x10000000
private const val WRITE_POS = 1
private const val READ_POS = 0
private val binaryName = fetchBinaryPath(getpid().toUInt()).fileName

private fun handleChildProcess(fileDescriptors: CArrayPointer<IntVar>, namespaceArgs: NamespaceArguments) {
    close(fileDescriptors[READ_POS])
    // Redirect standard output to the pipe.
    duplicateFileDescriptor(fileDescriptors[WRITE_POS], STDOUT_FILENO)
    // Redirect standard error to the pipe.
    duplicateFileDescriptor(fileDescriptors[WRITE_POS], STDERR_FILENO)
    runProgram(namespaceArgs.programPath, namespaceArgs.programArgs.toTypedArray())
}

private fun runFunction(argsPtr: COpaquePointer?): Int = memScoped {
    val args = argsPtr?.asStableRef<NamespaceArguments>()?.get()
        ?: throw IllegalStateException("NamespaceArguments cannot be null")
    val status = alloc<IntVar>()
    val fileDescriptors = allocArray<IntVar>(2)
    createPipe(fileDescriptors)
    val pid = fork()
    val outputFile = openFile(args.outputFilePath, "w")

    if (pid == 0) handleChildProcess(fileDescriptors, args)
    // Close unused write end of pipe. In some cases processes will deadlock without this.
    close(fileDescriptors[WRITE_POS])
    val inputFile = openFileDescriptor(fileDescriptors[READ_POS])
    inputFile.readLines(10u).forEach { outputFile.writeLine(it) }
    outputFile.close()
    inputFile.close()
    waitpid(__options = 0, __pid = pid, __stat_loc = status.ptr)
    status.value
}

private fun createNamespace(
    arg: StableRef<NamespaceArguments>,
    stack: COpaquePointer?,
    stackSize: ULong
) = clone_process(
    flags = CLONE_NEWPID or CLONE_NEWUTS or CLONE_NEWUSER or SIGCHLD,
    arg = arg.asCPointer(),
    stackSize = stackSize,
    stack = stack,
    fn = staticCFunction(::runFunction)
)

internal suspend fun runInNamespace(namespaceArgs: NamespaceArguments): Pair<Int, Array<String>> = memScoped {
    val status = alloc<IntVar>()
    val stableRef = StableRef.create(namespaceArgs)
    val stack = platform.linux.malloc(STACK_SIZE)
    val pid = createNamespace(arg = stableRef, stack = stack, stackSize = STACK_SIZE)

    checkPid(pid, stack)
    addRunningFunction(pid.toUInt(), namespaceArgs.programPath.fileName)
    runProcessTimer(pid = pid.toUInt(), binaryName = binaryName, duration = namespaceArgs.maxFuncDuration)
    waitpid(__options = 0, __pid = pid, __stat_loc = status.ptr)
    removeRunningFunction(pid.toUInt())
    free(stack)
    stableRef.dispose()
    val file = openFile(namespaceArgs.outputFilePath, "r")
    val output = file.readLines(10u)
    file.close()
    status.value to output
}

private fun checkPid(pid: Int, stack: COpaquePointer?) {
    if (pid < 0) {
        free(stack)
        throw IllegalStateException("Invalid PID")
    }
}
