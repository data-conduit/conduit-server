package org.dataConduit.conduitServer.io

import kotlin.test.Test
import kotlin.test.assertEquals

class PathsTest {
    @Test
    fun `toString returns the correct value`() {
        val path = "/tmp/test.txt".toPath()
        assertEquals(expected = "/tmp/test.txt", actual = "$path")
    }

    @Test
    fun `fromString creates a valid path`() {
        assertEquals(expected = "/tmp/test", actual = "${Path.fromString(" /tmp/../test/ ")}")
    }

    @Test
    fun `toPath creates a valid path`() {
        assertEquals(expected = "/tmp/test", actual = "${" /tmp/../test/ ".toPath()}")
    }

    @Test
    fun `appendItems creates a valid path`() {
        val path = "/tmp"
            .toPath()
            .appendItems(" ", "..", "/", " ", "//", "test", " ")
        assertEquals(expected = "/tmp/test", actual = "$path")
    }

    @Test
    fun `fileExt returns a valid file extension`() {
        assertEquals(expected = "txt", actual = "/tmp/test.txt".toPath().fileExt)
        assertEquals(expected = "txt", actual = "test.txt".toPath().fileExt)
        assertEquals(expected = "", actual = "text_file".toPath().fileExt)
    }

    @Test
    fun `fileName returns a valid file name`() {
        assertEquals(expected = "dog.jpg", actual = "/tmp/images/dog.jpg".toPath().fileName)
        assertEquals(expected = "dog.jpg", actual = "dog.jpg".toPath().fileName)
    }

    @Test
    fun `parentDir returns a valid parent directory`() {
        assertEquals(expected = "/tmp/images", actual = "/tmp/images/dog.jpg".toPath().parentDir)
        assertEquals(expected = "", actual = "test.txt".toPath().parentDir)
        assertEquals(expected = "/tmp", actual = "/tmp/text_file".toPath().parentDir)
    }
}
