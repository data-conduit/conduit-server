package org.dataConduit.conduitServer.webClient

import io.kvision.rest.HttpMethod
import io.kvision.rest.ResponseBodyType
import io.kvision.rest.RestClient
import io.kvision.rest.call
import kotlinx.coroutines.await

private val restClient = RestClient()
private const val HOST = "localhost"

// TODO: Obtain port number.
private const val PORT = 9000
private const val TEXT_TYPE = "text/plain"

internal suspend fun listInstalledFunctions(): Array<String> =
    restClient.call<Array<String>>("http://$HOST:$PORT/function/all/list-installed").await()

internal suspend fun runFunction(name: String, args: String = ""): String {
    var result = ""
    restClient.call<String, String>(
        url = "http://$HOST:$PORT/function/run/$name",
        data = args
    ) {
        method = HttpMethod.POST
        contentType = TEXT_TYPE
        responseBodyType = ResponseBodyType.TEXT
    }.then {
        result = it
    }.catch {
        result = "Failed to run function: ${it.message}"
    }.await()
    return result
}
