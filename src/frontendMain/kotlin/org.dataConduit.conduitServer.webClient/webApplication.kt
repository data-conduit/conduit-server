package org.dataConduit.conduitServer.webClient

import io.kvision.*
import org.dataConduit.conduitServer.webClient.view.createMainPage

class WebApplication : Application() {
    override fun start() {
        createMainPage()
    }
}

fun main() {
    startApplication(
        ::WebApplication,
        module.hot,
        BootstrapModule,
        BootstrapCssModule,
        CoreModule
    )
}
