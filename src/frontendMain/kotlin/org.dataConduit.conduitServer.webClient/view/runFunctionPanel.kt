package org.dataConduit.conduitServer.webClient.view

import io.kvision.core.Container
import io.kvision.form.text.Text
import io.kvision.form.text.TextArea
import io.kvision.form.text.text
import io.kvision.form.text.textArea
import io.kvision.html.button
import io.kvision.html.label
import io.kvision.panel.hPanel
import io.kvision.panel.vPanel
import io.kvision.utils.em
import io.kvision.utils.perc
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.dataConduit.conduitServer.webClient.runFunction

private lateinit var outputTxtArea: TextArea
private lateinit var funcArgsTxtArea: TextArea
private lateinit var funcNameTxt: Text

internal fun Container.runFunctionPanel() {
    vPanel(spacing = 5) {
        hPanel(spacing = 6) {
            marginTop = 1.2.em
            label("Function Name")
            funcNameTxt = text()
        }
        funcArgsTxtArea()
        runBtn()
        outputTxtArea()
    }
}

private fun Container.funcArgsTxtArea() {
    funcArgsTxtArea = textArea(label = "Function Arguments") { cols = 5 }
}

private fun Container.runBtn() {
    button("Run") {
        marginTop = 0.5.em
        width = 10.perc
        onClick {
            outputTxtArea.value = ""
            disabled = true
            // TODO: Launch Coroutine in a custom Coroutine Scope.
            GlobalScope.launch {
                val name = funcNameTxt.value ?: ""
                val args = funcArgsTxtArea.value ?: ""
                outputTxtArea.value = runFunction(name, args)
                disabled = false
            }
        }
    }
}

private fun Container.outputTxtArea() {
    outputTxtArea = textArea(label = "Output") {
        marginTop = 2.0.em
        marginRight = 2.0.em
        height = 30.perc
        readonly = true
    }
}
