package org.dataConduit.conduitServer.webClient.view

import io.kvision.core.Container
import io.kvision.html.*
import io.kvision.panel.vPanel
import io.kvision.utils.em
import io.kvision.utils.perc
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.dataConduit.conduitServer.webClient.listInstalledFunctions

private lateinit var list: Ul

internal fun Container.installedFunctionsPanel() {
    vPanel(spacing = 2) {
        refreshBtn()
        list = ul { marginTop = 1.2.em }
        refreshList()
    }
}

private fun Container.refreshBtn() {
    button("Refresh") {
        marginTop = 0.5.em
        size = ButtonSize.SMALL
        width = 8.perc
        onClick { refreshList() }
    }
}

private fun refreshList() {
    list.removeAll()
    // TODO: Launch Coroutine in a custom Coroutine Scope.
    GlobalScope.launch {
        listInstalledFunctions().forEach { item -> list.add(Li(item)) }
    }
}
