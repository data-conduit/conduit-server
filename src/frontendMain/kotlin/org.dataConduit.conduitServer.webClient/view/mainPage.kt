package org.dataConduit.conduitServer.webClient.view

import io.kvision.Application
import io.kvision.core.Container
import io.kvision.html.h1
import io.kvision.panel.root
import io.kvision.panel.tab
import io.kvision.panel.tabPanel
import io.kvision.utils.em

internal fun Application.createMainPage() = root("kvapp") {
    marginTop = 2.2.em
    marginLeft = 2.2.em
    h1("Conduit Server")
    tabLayout()
}

private fun Container.tabLayout() {
    tabPanel {
        marginTop = 1.5.em
        tab(label = "Installed Functions") { installedFunctionsPanel() }
        tab(label = "Run Function") { runFunctionPanel() }
    }
}
