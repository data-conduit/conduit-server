package org.dataConduit.conduitServer

import io.ktor.utils.io.*
import org.dataConduit.conduitServer.io.Path

internal expect val functionsDir: Path

internal expect suspend fun installFunction(name: String, channel: ByteReadChannel, size: UInt)

internal expect fun uninstallFunction(name: String)

internal expect suspend fun ByteReadChannel.readBinary(size: UInt): ByteArray

internal expect fun createFunctionDirectory(name: String)

internal expect fun functionExists(name: String): Boolean

internal expect fun listInstalledFunctions(): Array<String>
