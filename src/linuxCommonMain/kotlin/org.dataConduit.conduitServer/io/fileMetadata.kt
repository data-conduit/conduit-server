package org.dataConduit.conduitServer.io

internal data class FileMetadata(
    /** User identifier. */
    val userId: UInt,
    /** Group identifier. */
    val groupId: UInt,
    /** ID of device containing file. */
    val devId: ULong,
    /** File type and mode. */
    val mode: UInt,
    /** Total number of hard links. */
    val hardLinks: ULong,
    /** Inode number. */
    val inodeNum: ULong,
    /** Device ID (if special file). */
    val specialDevId: ULong,
    /** Total size in bytes. */
    val totalSize: Long,
    /** Block size for filesystem I/O. */
    val blockSize: Long,
    /** Number of 512 byte blocks allocated. */
    val totalBlocks: Long,
    /** Time of last access in seconds. */
    val lastAccessTime: Long,
    /** Time of last modification in seconds. */
    val lastModificationTime: Long,
    /** Time of last status change in seconds. */
    val lastStatusChangeTime: Long
)

internal fun emptyFileMetadata() = FileMetadata(
    userId = 0u,
    groupId = 0u,
    devId = 0uL,
    mode = 0u,
    hardLinks = 0uL,
    inodeNum = 0uL,
    specialDevId = 0uL,
    totalSize = 0L,
    blockSize = 0L,
    totalBlocks = 0L,
    lastStatusChangeTime = 0L,
    lastModificationTime = 0L,
    lastAccessTime = 0L
)
