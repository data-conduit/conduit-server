package org.dataConduit.conduitServer.io

internal expect fun deleteDirectory(dirPath: Path)

internal expect fun listDirectories(dirPath: Path): Array<String>

internal expect fun createBinaryFile(filePath: Path, data: ByteArray)

internal expect fun duplicateFileDescriptor(oldFd: Int, newFd: Int)

internal expect fun removeFile(filePath: Path)

internal expect fun createDirectory(dirPath: Path, mode: UInt)
