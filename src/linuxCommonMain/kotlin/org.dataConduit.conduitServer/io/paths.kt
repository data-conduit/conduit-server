package org.dataConduit.conduitServer.io

internal const val DIR_SEPARATOR = '/'
internal const val FILE_EXT_SEPARATOR = '.'

internal class Path private constructor(val value: String) {
    val parentDir: String
        get() {
            val separatorFound = value.lastIndexOf(DIR_SEPARATOR) != -1
            val endPos = value.lastIndexOf(DIR_SEPARATOR) - 1
            val startPos = 0
            return if (!separatorFound) "" else value.slice(startPos..endPos)
        }
    val fileName: String
        get() {
            val separatorFound = value.lastIndexOf(DIR_SEPARATOR) != -1
            val startPos = value.lastIndexOf(DIR_SEPARATOR) + 1
            val endPos = value.length - 1
            return if (!separatorFound) value else value.slice(startPos..endPos)
        }
    val fileExt: String
        get() {
            val dotFound = value.lastIndexOf(FILE_EXT_SEPARATOR) != -1
            val startPos = value.lastIndexOf(FILE_EXT_SEPARATOR) + 1
            val endPos = value.length - 1
            return if (!dotFound) "" else value.slice(startPos..endPos)
        }

    fun appendItems(vararg items: String): Path = buildString {
        append(value)
        items.forEach { i ->
            val newItem = i
                .trim()
                .replace("..", "")
                .replace("$DIR_SEPARATOR", "")
                .replace("$DIR_SEPARATOR$DIR_SEPARATOR", "")
            if (newItem.isNotEmpty()) append("$DIR_SEPARATOR$newItem")
        }
    }.toPath()

    override fun toString(): String = value

    companion object {
        fun fromString(value: String): Path {
            val tmp = value.trim()
                .replace("..", "")
                .replace("$DIR_SEPARATOR$DIR_SEPARATOR", "$DIR_SEPARATOR")
                .run { trimEnd { it == DIR_SEPARATOR } }
            return Path(tmp)
        }
    }
}

internal fun String.toPath() = Path.fromString(this)

internal expect fun Path.fileExists(): Boolean

internal expect fun Path.directoryExists(): Boolean

internal expect val Path.metadata: FileMetadata
