import org.jetbrains.kotlin.gradle.targets.js.webpack.KotlinWebpackConfig

group = "org.data-conduit"
version = "0.2"

plugins {
    val kotlinVer: String by System.getProperties()
    val kvisionVer: String by System.getProperties()

    kotlin("multiplatform") version kotlinVer
    kotlin("plugin.serialization") version kotlinVer
    id("io.kvision") version kvisionVer
}

repositories {
    mavenCentral()
}

val ktorVer: String by System.getProperties()
val coroutinesVer: String by System.getProperties()
val kotlinVer: String by System.getProperties()
val kvisionVer: String by System.getProperties()
val webDir = file("src/frontendMain/web")
val webAssetsDir = file("$buildDir/bin/linuxX64/conduit_serverDebugExecutable/static")
val webDistributionDir = file("$buildDir/distributions")
val ktorGroupId = "io.ktor"

kotlin {
    linuxX64 {
        compilations.getByName("test") {
            dependencies {
                implementation(kotlin("test", kotlinVer))
            }
        }
        compilations.getByName("main") {
            cinterops.create("scheduler")
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVer")
                implementation("$ktorGroupId:ktor-server-core:$ktorVer")
                implementation("$ktorGroupId:ktor-server-cio:$ktorVer")
                implementation("$ktorGroupId:ktor-serialization-kotlinx-json:$ktorVer")
                implementation("$ktorGroupId:ktor-server-content-negotiation:$ktorVer")
            }
        }
        binaries {
            executable("conduit_server") {
                entryPoint = "org.dataConduit.conduitServer.main"
            }
        }
    }

    js("frontend") {
        compilations.getByName("main").dependencies {
            val kvisionGroupId = "io.kvision"
            implementation("$kvisionGroupId:kvision:$kvisionVer")
            implementation("$kvisionGroupId:kvision-bootstrap:$kvisionVer")
            implementation("$kvisionGroupId:kvision-bootstrap-css:$kvisionVer")
            implementation("$kvisionGroupId:kvision-state:$kvisionVer")
            implementation("$kvisionGroupId:kvision-rest:$kvisionVer")
        }

        browser {
            runTask {
                outputFileName = "web_client.bundle.js"
                sourceMaps = false
                devServer = KotlinWebpackConfig.DevServer(
                    open = false,
                    port = 3000,
                    proxy = mutableMapOf(
                        "/kv/*" to "http://localhost:8080",
                        "/kvws/*" to mapOf("target" to "ws://localhost:8080", "ws" to true)
                    ),
                    static = mutableListOf("$buildDir/processedResources/frontend/main")
                )
            }
            commonWebpackConfig {
                outputFileName = "web_client.bundle.js"
            }
            testTask {
                useKarma {
                    useChromeHeadless()
                }
            }
        }
        binaries.executable()
    }

    sourceSets {
        commonMain {
            dependencies {
                implementation(kotlin("stdlib", kotlinVer))
                implementation("$ktorGroupId:ktor-serialization-kotlinx-json:$ktorVer")
            }
        }
        val linuxCommonMain by creating {
            dependencies {
                implementation("$ktorGroupId:ktor-server-core:$ktorVer")
            }
        }
        @Suppress("UNUSED_VARIABLE")
        val linuxX64Main by getting {
            dependsOn(linuxCommonMain)
        }
        @Suppress("UNUSED_VARIABLE")
        val frontendMain by getting {
            resources.srcDir(webDir)
        }
    }
}

val copyHtmlFiles by tasks.creating(Copy::class) {
    dependsOn("frontendBrowserDistribution")
    include("*.html")
    from(webDistributionDir)
    into(webAssetsDir)
}

val copyJsFiles by tasks.creating(Copy::class) {
    dependsOn("frontendBrowserDistribution")
    include("*.js")
    from(webDistributionDir)
    into(file("$webAssetsDir/js"))
}

tasks.create("createWebClient") {
    dependsOn(copyHtmlFiles, copyJsFiles)
}
