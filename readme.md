# Conduit Server (conduit-server)

Runs and manages Serverless Functions on demand. The server requires the Linux OS in order to run. Currently, only 
AMD64 (64 bit AMD/Intel CPUs) based devices are supported. The ARMv7/8 based devices will be supported in the future, 
however that requires the KotlinX Coroutines library to have support for the **linuxArm64** and **linuxArm32Hfp** 
Kotlin Native targets.

Conduit Server is written in Kotlin via Kotlin Native. Linux Namespaces are used for resource isolation (provides 
basic security).


## Installation

Copy the Conduit Server binary to the installation directory (eg **/usr/local/conduit_server**). In the installation 
directory create a new directory called **functions**, which will be the place where all Serverless Functions are 
stored.


## Usage

Conduit Server can run without any program arguments (aka zero configuration): `conduit_server`

The port can be set using the **--port** program argument, eg: 
```shell
conduit_server --port=9000
```

Maximum Serverless Function duration (in seconds) can be set using the **--maxFuncDuration=** program argument, eg: 
```shell
conduit_server --maxFuncDuration=240
```


## Serverless Functions

Each Serverless Function is placed in its own directory. Do note that a Serverless Function **MUST** be
represented as a binary, and the binary name **MUST** match the directory name. Below are some examples:
```
  /usr/local/conduit_server/functions/basic_func
  
  /usr/local/conduit_server/functions/weather_forcast
```

Refer to the [Basic Function sample](https://gitlab.com/data-conduit/samples/basic-function) on how to do a basic 
Serverless Function.


## Web API

An HTTP REST API is provided by Conduit Server. All examples will use **localhost** as the host, and **8080** as the 
port.

Checking if Conduit Server is running (**GET** method): `/`. If the server is running then a response will be returned 
with **Conduit Server** as the body.

Calling a Serverless Function (**POST** method): `/run-function/{func_name}`
  - Example: `http://localhost:8080/run-function/basic_func`

Getting metrics for a Serverless Function (**GET** method): `/function-metrics/{func_name}`
  - Example: `http://localhost:8080/function-metrics/basic_func`

List all running Serverless Functions (**GET** method): `/list-running-functions`
  - Example: `http://localhost:8080/list-running-functions`
